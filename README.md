

docker image for ngix
=====================

ref: https://www.digitalocean.com/community/tutorials/docker-explained-how-to-containerize-and-use-nginx-as-a-proxy



sandbox:

```
docker run --name some-nginx -v /some/content:/usr/share/nginx/html:ro -d nginx
```


main ngix volumes
=================
* VOLUME ["/usr/share/nginx/html"]
* VOLUME ["/etc/nginx"]


EXPOSE 80 443

